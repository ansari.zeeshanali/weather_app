defmodule Weather.Worker do

    def temperature_of_cities(cities) do
       pid = spawn(Weather.Coordinator,:loop, [[],Enum.count(cities)])
        cities |> Enum.each(fn city->
            worker_id= spawn(Weather.Worker,:loop, [])
            send(worker_id,{pid,city})
        end)
    end

    def loop() do
        receive do
            {pid,location}-> send(pid, temperature_of(location))
            _->:error
        end
        loop()
    end

    def temperature_of(location) do
        {:ok, temperature} = location
                        |>get_data
                        |>extract_temp
        {:ok, {location, "#{temperature - 273.15} C"}}
    end

    defp extract_temp( %HTTPoison.Response{body: body, status_code: 200}) do
        {:ok, data}=JSON.decode(body)
        {:ok, data["main"]["temp"]}
    end

    defp extract_temp(_) do
        :error
    end

    defp get_data(location) do
        api_key="3aa40e5d964ef86bf7656a5cdf8f545a"
        url = "http://api.openweathermap.org/data/2.5/weather?q=#{location}&appid=#{api_key}"
        HTTPoison.get!(url)
    end

end
