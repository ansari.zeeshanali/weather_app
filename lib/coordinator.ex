defmodule Weather.Coordinator do
    def loop(results \\[], expected_count) do
        receive do
            {:ok, result}->
                new_results = [result|results]
                if expected_count == Enum.count(new_results) do
                    send(self(), :exit)
                end
                loop(new_results,expected_count)
            :exit -> IO.inspect(results)
            _ -> loop(results, expected_count)    
        end
    end
end